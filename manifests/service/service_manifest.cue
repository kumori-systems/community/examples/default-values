package service

import (
  f ".../frontend:component"
)

#Artifact: {
  ref: name:  "service"

  description: {

    config: parameter: {

      myServConfig: {
        // There are no default value for myServParam1, at service level. So
        // it is mandatory that the deployment manifest provides a value for
        // this parameter.
        myServParam1: string

        // There are a default value for myServParam2, at service level. This
        // value will be used if no value is assigned when the service is deployed.
        myServParam2: string | *"default-in-service-2"


        // There are no default value for myServParam3, at service level. But
        // we don't want to force this parameter to appear in the deployment
        // manifest, because we know that there is a default value at the
        // component level. To indicate its optionality, we use the `?` symbol.
        myServParam3?: string

        // The case of the myServParam4 parameter is special: *we know* that
        // there is a default value at the component level, but we want to
        // override it at the service level.
        myServParam4: string | *"default-in-service-4"
      }

      // Equivalent to myComponentBandwidth in the component configuration, with
      // no default values. It is an optional property (note the `?` symbol),
      // because we know that there is a default value at the component level.
      myComponentBandwidth?: number

      // Equivalent to myContainerMinCpu/Cpu/Memory in the component configuration,
      // with no default values and also optional (note the `?` symbol),
      // because we know that there is a default value at the component level.
      myContainerMinCpu?: number
      myContainerCpu?: number
      myContainerMemory?: number
    }

    let _parameter = description.config.parameter
    let _myServConfig = description.config.parameter.myServConfig

    role: frontend: {
      artifact: f.#Artifact

      // Spread
      config: parameter: {

        myConfig: {

          // myParam1 takes it value from myServParam2
          myparam1: _myServConfig.myServParam1

          // myParam2 takes it value from `myServParam2` (the deployment value,
          // if defined, or its service default value).
          myparam2: _myServConfig.myServParam2

          // myParam3 takes it value from `myServParam3` only if it has assigned
          // a value. In other case, myParam3 is not setted in the spread, and
          // its component default value will be used.
          if _myServConfig.myServParam3 != _|_ {
            myparam3: _myServConfig.myServParam3
          }

          // myParam4 takes it value from `myServParam4` (the deployment value,
          // if defined, or its service default value). The default value
          // defined at component level will be ignored.
          myparam4: _myServConfig.myServParam4
        }

        // The behaviour of myComponentBandwidth and myContainerSize is
        // equivalent to myparam3
        if _parameter.myComponentBandwidth != _|_ {
          myComponentBandwidth: _parameter.myComponentBandwidth
        }
        if _parameter.myContainerMinCpu != _|_ {
          myContainerMinCpu: _parameter.myContainerMinCpu
        }
        if _parameter.myContainerCpu != _|_ {
          myContainerCpu: _parameter.myContainerCpu
        }
        if _parameter.myContainerMemory != _|_ {
          myContainerMemory: _parameter.myContainerMemory
        }
      }
    }

    srv: server: restapi: { protocol: "http", port: 80 }
    connect: {
      inbound: {
        as: "lb"
  			from: self: "restapi"
        to: frontend: "restapi": _
      }
    }
  }
}
