package component

#Artifact: {
  ref: name:  "frontend"

  description: {

    srv: {
      server: restapi: { protocol: "http", port: 8080 }
    }

    config: parameter: {

      myConfig: {
        // myParam1 has no default values, neither at component level nor at
        // service level.
        myparam1: string

        // myParam2 has no default values at component level, but a default value
        // exists at service level.
        myparam2: string

        // myParam3 has a default value at component level
        myparam3: string | *"default-in-component-3"

        // myParam4 has two default values: one at the component level and the
        // other  (which must predominate) at the service level.
        myparam4: string | *"default-in-component-4"
      }

      // myComponentBandwidth is the number of Mbps (without indicating the
      // units), and it has a default value at compoment level
      myComponentBandwidth: number | *10

      // myContainerMinCpu, myContainerCpu and myContainerMem are the CPU and
      // mem in the format used to specify container size, and they have a default
      // value at compoment level
      myContainerMinCpu: number | *100
      myContainerCpu: number | *150
      myContainerMemory: number | *100
    }

    // myComponentBandwidth parameter is used to compose the property that sets
    // the machine resources at component level. In this example, the units (Mbps)
    // cannot be changed.
    size: bandwidth: {
      size: description.config.parameter.myComponentBandwidth
      unit: "M"
    }

    code: frontend: {
      name: "frontend"

      image: {
        hub: { name: "", secret: "" }
        tag: "kumoripublic/examples-service-configuration-frontend:v1.0.4"
      }

      // The container size is derived from myContainerCpu and myContainerMemory
      // parameters. In this example, the units cannot be changed.
      size: {
        memory: {
          size: description.config.parameter.myContainerMemory
          unit: "Mi"
        }
        mincpu: description.config.parameter.myContainerMinCpu
        cpu: {
          size: description.config.parameter.myContainerCpu
          unit: "m"
        }
      }

      mapping: {
        filesystem: {
          "/kumori/files/example.json": {
            data: value: {
              myconfig: description.config.parameter.myConfig
              containerSize: description.code.frontend.size
              componentSize: description.size
            }
            format: "json"
          }
        }
        env: {
          HTTP_SERVER_PORT_ENV: value: "\(srv.server.restapi.port)"
        }
      }
    }
  }
}
