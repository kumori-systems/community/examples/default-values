package deployment

import s ".../service:service"

#Deployment: {
  name: "configexampledep"
  artifact: s.#Artifact
  config: {

    // In this case, all the configuration parameters are specified, so no
    // default values will be used
    parameter: {
      myServConfig: {
        myServParam1: "value1"
        myServParam2: "value2"
        myServParam3: "value3"
        myServParam4: "value4"
      }
      myComponentBandwidth: 20
      myContainerMinCpu: 200
      myContainerCpu: 250
      myContainerMemory: 200
    }

    scale: detail: frontend: hsize: 2
    resilience: 1
  }
}
