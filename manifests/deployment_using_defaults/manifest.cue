package deployment

import s ".../service:service"

#Deployment: {
  name: "configexampledep"
  artifact: s.#Artifact
  config: {

    // In this case, only the mandatory configuration parameters are specified,
    // so default values will be used
    parameter: {
      myServConfig: {
        myServParam1: "value1"
      }
    }

    scale: detail: frontend: hsize: 2
    resilience: 1
  }
}