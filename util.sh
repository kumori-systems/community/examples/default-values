#!/bin/bash

# Cluster variables
CLUSTERNAME="..."
REFERENCEDOMAIN="test.kumori.cloud"
CLUSTERCERT="cluster.core/wildcard-test-kumori-cloud"

# Service variables
INBOUNDNAME="defaultvaluesinb"
DEPLOYNAME="defaultvaluesdep"
DOMAIN="defaultvaluesdomain"
SERVICEURL="defaultvalues-${CLUSTERNAME}.${REFERENCEDOMAIN}"

KUMORI_SERVICE_MODEL_VERSION="1.1.6"

# Change if special binaries want to be used
KAM_CMD="kam"
KAM_CTL_CMD="kam ctl"

case $1 in

'refresh-dependencies')
  cd manifests
  # Dependencies are removed and added again just to ensure that the right versions
  # are used.
  # This is not necessary, if the versions do not change!
  ${KAM_CMD} mod dependency --delete kumori.systems/kumori
  ${KAM_CMD} mod dependency kumori.systems/kumori/@${KUMORI_SERVICE_MODEL_VERSION}
  # Just for sanitize: clean the directory containing dependencies
  rm -rf ./cue.mod
  cd ..
  ;;

'create-domain')
  ${KAM_CTL_CMD} register domain $DOMAIN --domain $SERVICEURL
  ;;

'deploy-inbound')
  ${KAM_CTL_CMD} register inbound $INBOUNDNAME \
    --domain $DOMAIN \
    --cert $CLUSTERCERT
  ;;

# To check if our module is correct (from the point of view of the CUE syntax and
# the Kumori service model), we can use "kam process" to enerate the JSON
# representation
'dry-run-default')
  time ${KAM_CMD} process deployment_using_defaults -t ./manifests
  ;;

'dry-run-setting')
  time ${KAM_CMD} process deployment_setting_values -t ./manifests
  ;;

'deploy-service')
  ${KAM_CMD} service deploy -d deployment_using_defaults -t ./manifests $DEPLOYNAME -- \
    --comment "Using default values" \
    --wait 5m
  ;;

'link')
  ${KAM_CTL_CMD} link $DEPLOYNAME:restapi $INBOUNDNAME:inbound
  ;;

'deploy-all')
  $0 create-domain
  $0 deploy-inbound
  $0 deploy-service
  $0 link
  ;;

'update-service')
  ${KAM_CMD} service update -d deployment_setting_values -t ./manifests $DEPLOYNAME -- \
    --comment "Setting values" \
    --wait 5m
  ;;

'describe')
  ${KAM_CMD} service describe $DEPLOYNAME
  echo
  echo
  ${KAM_CMD} service describe $INBOUNDNAME
  ;;

'test')
  curl https://${SERVICEURL}/file/example.json; echo
  ;;

'unlink')
  ${KAM_CTL_CMD} unlink $DEPLOYNAME:restapi $INBOUNDNAME:inbound
  ;;

'undeploy-service')
  ${KAM_CMD} service undeploy $DEPLOYNAME -- --wait 5m --force
  ;;

'undeploy-inbound')
  ${KAM_CMD} service undeploy $INBOUNDNAME -- --wait 5m
  ;;

'delete-domain')
  ${KAM_CTL_CMD} unregister domain $DOMAIN
  ;;

'undeploy-all')
  $0 unlink
  $0 undeploy-service
  $0 undeploy-inbound
  $0 delete-domain
  ;;

*)
  echo "This script doesn't contain that command"
	;;

esac
